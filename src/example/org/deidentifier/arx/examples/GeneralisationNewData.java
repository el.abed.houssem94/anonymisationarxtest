package org.deidentifier.arx.examples;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * Simple Java program to read CSV file in Java. In this program we will read
 * list of books stored in CSV file as comma separated values.
 * 
 * @author WINDOWS 8
 *
 */
public class GeneralisationNewData {
    
    //File name
    private static final String FILE_NAME = "data/adultDataTest.csv";
    private static final String DESTINATION_FILE_NAME = "data/dataTestRes.csv";

    //Delimiter used in CSV file
	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";        
	private static final String FILE_HEADER = "age,workclass,fnlwgt,education,education-num,marital-status,"
                + "occupation,relationship,race,sex,capital-gain,capital-loss,hours-per-week,"
                + "native-country,income";
    //countries sorted by continent
    static String[] EuropeCountries = {"Italy", "France", "Germany", "Portugal", "Greece", "Hungary", "Ireland", "England",
        "Holand-Nether", "Yugoslavia", "Scotland", "Poland"};
    
    static String[] AsiaCountries = {"Vietnam", "Thailand", "Philippines", "India", "Japan", "China", "Iran", "Cambodia",
        "Laos", "Hong", "South-Taiwan"};
    
    static String[] NorthAmericaCountries = {"Dominican-Rep", "El-Salvador", "Guatemala", "Honduras", "Haiti", "Jamaica",
        "Nicaragua", "Outlying-US", "Puerto-Rico", "United-States", "Trinadad&Toba",  "Columbia",  "Canada",  "Cuba",
        "Mexico"};
    
    static String[] SouthAmericaCountries = {"Peru", "Ecuador"};
    
    // posible marital status 
    static String[] mariedStatus = {"Married-AF-spouse", "Married-civ-spouse" ,"Married-spouse-absent" ,"Widowed"};
    static String[] notMarriedStatus = {"Divorced" , "Never-married", "Separated"};
    
    public static void main(String... args) {
        readBooksFromCSV(FILE_NAME);
    }

    private static void readBooksFromCSV(String fileName) {
        List<String[]> data = new ArrayList<String[]>();
        Path pathToFile = Paths.get(fileName);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,
                StandardCharsets.US_ASCII)) {
            // read the first line from the text file
            String line = br.readLine();
            String[] attributesNames = line.split(";");
           line = br.readLine();
            String[] attributes = {};
            
            // loop until all lines are read
            while (line != null ) {
                attributes = line.split(";");             
                
                //*********************Begin age generalization**********************
                attributes = ageGeneralisation(attributes);
                //*********************End age generalization************************
                                
                //*********************Begin Race generalization*********************
                attributes = raceGeneralisation(attributes);
                //*********************End Race generalization***********************
                
                //*********************Begin nativeCountry generalization*************
                attributes = nativeCountryGeneralisation(attributes);
                //*********************End nativeCountry generalization***************
                
                //*********************Begin nativeCountry generalization*************
                attributes = maritalStatusGeneralisation(attributes);
                //*********************End nativeCountry generalization***************
                data.add(attributes);
                line = br.readLine();
                //System.out.print(attributes[1]+" -- "+attributes[2]+" -- "+attributes[5]+" -- "+attributes[3]);
                //for (String att : attributes) 
                    //System.out.print(att);
                //System.out.println("");
            }
            writeCsvFile(DESTINATION_FILE_NAME, data);
            
            

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    public static String[] ageGeneralisation(String[] attribute){
        String [] attributes = attribute;
        if(Integer.parseInt(attributes[0])<=40){
                    attributes[1] = "age<=40";
        }else{
                    if (40<Integer.parseInt(attributes[0])&&Integer.parseInt(attributes[0])<70){
                        attributes[0] = "40<age<70";
                    }else{
                        attributes[0] = ">=70";
                    }
        }
        return attributes; 
    }
    
    public static String[] raceGeneralisation(String[] attribute){
        String [] attributes = attribute;
        if(attributes[8].equals("Black")){
                attributes[8] = "African";
        }else{
                if(attributes[8].equals("White")){
                    attributes[8] = "European";
                }else{
                    attributes[8] = "Other";
                }
        }
        return attributes; 
    }
    
    public static String[] nativeCountryGeneralisation(String[] attribute){
        String [] attributes = attribute;
        int i = 0;
                boolean coutryFounded = false;
                while(i<EuropeCountries.length && !coutryFounded){
                    if(attributes[13].equals(EuropeCountries[i])){
                        attributes[13] = "Europe";
                        coutryFounded = true;                        
                    }
                    i++;
                }
                i= 0;
                while(i<AsiaCountries.length && !coutryFounded){
                    if(attributes[13].equals(AsiaCountries[i])){
                        attributes[13] = "Asia";
                        coutryFounded = true;
                    }
                    i++;
                }
                i= 0;
                while(i<NorthAmericaCountries.length && !coutryFounded){
                    if(attributes[13].equals(NorthAmericaCountries[i])){
                        attributes[13] = "North america";
                        coutryFounded = true;
                    }
                    i++;
                }
                i= 0;
                while(i<SouthAmericaCountries.length && !coutryFounded){
                    if(attributes[13].equals(SouthAmericaCountries[i])){
                        attributes[13] = "South America";
                        coutryFounded = true;
                    }
                    i++;
                }
        return attributes; 

    }
     
    public static String[] maritalStatusGeneralisation(String[] attribute){
        String [] attributes = attribute;
        int i = 0;
        boolean maritalStatusFounded = false;
        while(i<mariedStatus.length && !maritalStatusFounded){
            if(attributes[5].equals(mariedStatus[i])){
                attributes[5] = "Married";
                maritalStatusFounded = true;
            }
            i++;
        }
        i= 0;
        while(i<notMarriedStatus.length && !maritalStatusFounded){
            if(attributes[5].equals(notMarriedStatus[i])){
                attributes[5] = "Not married";
                maritalStatusFounded = true;
            }
            i++;
        }
        
        
    return attributes; 

    } 
    
    public static void writeCsvFile(String fileName, List<String[]> data) {
        FileWriter fileWriter = null;
        try {
                fileWriter = new FileWriter(fileName);

                //Write the CSV file header
                fileWriter.append(FILE_HEADER.toString());

                //Add a new line separator after the header
                fileWriter.append(NEW_LINE_SEPARATOR);

                //Write a new student object list to the CSV file
                for (String[] attributes : data) {
                    for (int i = 0; i < attributes.length; i++) {
                        fileWriter.append(attributes[i]);
                        if(i!=(attributes.length-1)){
                            fileWriter.append(COMMA_DELIMITER);
                        }
                    }
                fileWriter.append(NEW_LINE_SEPARATOR);
                }



                System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
                System.out.println("Error in CsvFileWriter !!!");
                e.printStackTrace();
        } finally {

                try {
                        fileWriter.flush();
                        fileWriter.close();
                } catch (IOException e) {
                        System.out.println("Error while flushing/closing fileWriter !!!");
        e.printStackTrace();
                }

        }
    }
}