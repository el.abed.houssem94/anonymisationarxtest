/*
 * ARX: Powerful Data Anonymization
 * Copyright 2012 - 2018 Fabian Prasser and contributors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.deidentifier.arx.examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.deidentifier.arx.ARXAnonymizer;
import org.deidentifier.arx.ARXConfiguration;
import org.deidentifier.arx.ARXResult;
import org.deidentifier.arx.AttributeType;
import org.deidentifier.arx.AttributeType.Hierarchy;
import org.deidentifier.arx.AttributeType.Hierarchy.DefaultHierarchy;
import org.deidentifier.arx.Data;
import org.deidentifier.arx.Data.DefaultData;
import org.deidentifier.arx.DataSubset;
import org.deidentifier.arx.aggregates.HierarchyBuilderRedactionBased;
import org.deidentifier.arx.aggregates.HierarchyBuilderRedactionBased.Order;
import org.deidentifier.arx.criteria.DPresence;
import org.deidentifier.arx.criteria.HierarchicalDistanceTCloseness;
import org.deidentifier.arx.criteria.KAnonymity;
import org.deidentifier.arx.criteria.RecursiveCLDiversity;
import static org.deidentifier.arx.examples.AnotherTestForAnonymization.educationHierarchy;
import static org.deidentifier.arx.examples.AnotherTestForAnonymization.nativeCountryHierarchy;
import static org.deidentifier.arx.examples.AnotherTestForAnonymization.raceHierarchy;
import static org.deidentifier.arx.examples.Example.printResult;
import static org.deidentifier.arx.examples.Generalisation.EuropeCountries;
import org.deidentifier.arx.io.CSVHierarchyInput;
import org.deidentifier.arx.metric.Metric;

/**
 * This class implements an simple example for using multiple sensitive attributes and
 * different privacy models.
 *
 * @author Fabian Prasser
 * @author Florian Kohlmayer
 */
public class AnonymisationPhases extends Example  {

    private static final String FILE_NAME = "data/dataForAnonymization.csv";
    private static Hierarchy ssnn;
    private static Hierarchy namee;
    private static Hierarchy phone;
    private static Hierarchy mail;
    private static Hierarchy datee;
    private static Hierarchy countryy;
    private static Hierarchy cityy;
    private static Hierarchy zipcodee;
    private static Hierarchy streett;
    
    private static CSVHierarchyInput nameCSVHierarchyInput;
    private static DefaultHierarchy name = Hierarchy.create();
    private static DefaultHierarchy country = Hierarchy.create();
    private static DefaultHierarchy city = Hierarchy.create();
    private static DefaultHierarchy zipcode = Hierarchy.create();
    private static DefaultHierarchy street_address = Hierarchy.create();
    private static DefaultHierarchy ssn = Hierarchy.create();
    private static DefaultHierarchy phone_number_full = Hierarchy.create();
    private static DefaultHierarchy email = Hierarchy.create();
    private static DefaultHierarchy date = Hierarchy.create();

    // Define research subset
    private static DefaultData subsetData = Data.create();

    private static Identification identification = new Identification();

    private static String PATH_TO_THE_HIERARCHY = "/home/houssem/AlgoptisPFE/arx/data/DataHierarchy/";
    
    public static void main(String[] args) throws IOException {
        
        File filessn= new File(PATH_TO_THE_HIERARCHY+"ssnHierarchy.csv");
        File fileName= new File(PATH_TO_THE_HIERARCHY+"nameHierarchy.csv");
        File filePhoneNumber= new File(PATH_TO_THE_HIERARCHY+"phoneHierarchy.csv");
        File fileEmail= new File(PATH_TO_THE_HIERARCHY+"emailHierarchy.csv");
        File filedate= new File(PATH_TO_THE_HIERARCHY+"dateHierarchy.csv");
        File fileCountry= new File(PATH_TO_THE_HIERARCHY+"countryHierarchy.csv");
        File fileCity= new File(PATH_TO_THE_HIERARCHY+"cityHierarchy.csv");
        File fileZipcode= new File(PATH_TO_THE_HIERARCHY+"zipcodeHierarchy.csv");
        File fileStreet= new File(PATH_TO_THE_HIERARCHY+"streetHierarchy.csv");
        
        namee= Hierarchy.create(new CSVHierarchyInput(fileName, StandardCharsets.UTF_8, ',').getHierarchy());
        ssnn= Hierarchy.create(new CSVHierarchyInput(filessn, StandardCharsets.UTF_8, ',').getHierarchy());
        phone= Hierarchy.create(new CSVHierarchyInput(filePhoneNumber, StandardCharsets.UTF_8, ',').getHierarchy());
        mail= Hierarchy.create(new CSVHierarchyInput(fileEmail, StandardCharsets.UTF_8, ',').getHierarchy());
        datee= Hierarchy.create(new CSVHierarchyInput(filedate, StandardCharsets.UTF_8, ',').getHierarchy());
        countryy= Hierarchy.create(new CSVHierarchyInput(fileCountry, StandardCharsets.UTF_8, ',').getHierarchy());
        cityy= Hierarchy.create(new CSVHierarchyInput(fileCity, StandardCharsets.UTF_8, ',').getHierarchy());
        zipcodee= Hierarchy.create(new CSVHierarchyInput(fileZipcode, StandardCharsets.UTF_8, ',').getHierarchy());
        streett= Hierarchy.create(new CSVHierarchyInput(fileStreet, StandardCharsets.UTF_8, ',').getHierarchy());
            
        Data data = getData(FILE_NAME);
        //getHierarchyData(FILE_NAME);
        
        // Define research subset
        DataSubset subset = DataSubset.create(data, new HashSet<Integer>(randomListForSubset(50000)));
        // Define hierarchies
       
        /*
        HierarchyBuilderRedactionBased<?> builderName = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderCountry = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderCity = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderZipCode = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderStreet_address = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderSNN = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderPhone_number_full = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderEmail = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderDate = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        */
        
        data.getDefinition().setAttributeType("ssn", AttributeType.IDENTIFYING_ATTRIBUTE);
        data.getDefinition().setAttributeType("name", AttributeType.SENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("phone_number_full", AttributeType.IDENTIFYING_ATTRIBUTE);
        data.getDefinition().setAttributeType("email", AttributeType.IDENTIFYING_ATTRIBUTE);
        data.getDefinition().setAttributeType("date",datee);
        data.getDefinition().setAttributeType("country", countryy);
        data.getDefinition().setAttributeType("city", cityy);
        data.getDefinition().setAttributeType("zipcode",AttributeType.SENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("street_address", AttributeType.IDENTIFYING_ATTRIBUTE);
        
        

        // Create an instance of the anonymizer
        ARXAnonymizer anonymizer = new ARXAnonymizer();
        ARXConfiguration config = ARXConfiguration.create();
        config.addPrivacyModel(new KAnonymity(3));
        config.addPrivacyModel(new RecursiveCLDiversity("name",0.001, 4));
        config.addPrivacyModel(new HierarchicalDistanceTCloseness("zipcode", 0.2d,zipcodee));
        config.addPrivacyModel(new DPresence(1d / 2d, 2d / 3d, subset));
        config.setSuppressionLimit(0d);
        config.setQualityModel(Metric.createEntropyMetric());

        // Now anonymize
        ARXResult result = anonymizer.anonymize(data, config);

        // Print info
        printResult(result, data);

        // Process results
        if (result.getGlobalOptimum() != null) {
            System.out.println(" - Transformed data:");
            Iterator<String[]> transformed = result.getOutput(false).iterator();
            while (transformed.hasNext()) {
                System.out.print("   ");
                System.out.println(Arrays.toString(transformed.next()));
            }
        }
    }
    
    private static Data getData(String fileName) {
        DefaultData data = Data.create();
        Path pathToFile = Paths.get(fileName);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            String[] attributes = {};
            while (line != null ) {
                attributes = line.split(",");
                data.add(attributes);
                line = br.readLine();
            }
            
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return data;
    }
    
    private static void getHierarchyData(String fileName) {
        DefaultData data = Data.create();
        Path pathToFile = Paths.get(fileName);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            line = br.readLine();
            String[] attributes = {};
            while (line != null ) {
                attributes = line.split(",");
                ssn.add(generateHierarchy(attributes[0],0));
                name.add(generateHierarchy(attributes[1],1));
                phone_number_full.add(generateHierarchy(attributes[2],2));
                email.add(generateHierarchy(attributes[3],3));
                date.add(generateHierarchy(attributes[4],4));
                country.add(generateHierarchy(attributes[5],5));
                city.add(generateHierarchy(attributes[6],6));
                zipcode.add(generateHierarchy(attributes[7],7));
                street_address.add(generateHierarchy(attributes[8],8));
                
                
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static String[] generateHierarchy(String str, int column){
        StringBuilder sb = new StringBuilder(str);
        char ch='*';
	String[] hirearchyAnonymisation = new String[maxLengthForAllattributes(column)+1];
        hirearchyAnonymisation[0] = str;
        for (int i = 1;i < hirearchyAnonymisation.length ;i++) {
            if(i>str.length()){
                hirearchyAnonymisation[i] = "***************";
            }else{
                sb.setCharAt(i-1, ch);
                hirearchyAnonymisation[i] = sb.toString();
            }    
        }
        return hirearchyAnonymisation;
    }
   
    private static int maxLengthForAllattributes(int column){
        FileWriter fileWriter = null;
        int max=0;
        Path pathToFile = Paths.get(FILE_NAME);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            line = br.readLine();
            String[] attributes = {};
            while (line != null ) {
                attributes = line.split(",");
                if(attributes[column].length()>max){
                    max = attributes[column].length();
                }
                line = br.readLine();
            }
        }catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return max;
    }
    
    private static List<Integer> randomListForSubset(int r){
        List<Integer> sensitive = new ArrayList<Integer>(); 
        Random ran = new Random();
        int result =0;
        for (int i = 0; i < 3*r/4; i++) {
            result= ran.nextInt(r) ;
            if(sensitive.indexOf(result)==-1){
                sensitive.add(result);
            }
        }
        return sensitive;
    }
}
