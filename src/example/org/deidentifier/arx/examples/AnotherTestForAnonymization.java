/*
 * ARX: Powerful Data Anonymization
 * Copyright 2012 - 2018 Fabian Prasser and contributors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.deidentifier.arx.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.deidentifier.arx.ARXAnonymizer;
import org.deidentifier.arx.ARXConfiguration;
import org.deidentifier.arx.ARXResult;
import org.deidentifier.arx.AttributeType;
import org.deidentifier.arx.AttributeType.Hierarchy;
import org.deidentifier.arx.AttributeType.Hierarchy.DefaultHierarchy;
import org.deidentifier.arx.Data;
import org.deidentifier.arx.Data.DefaultData;
import org.deidentifier.arx.DataSubset;
import org.deidentifier.arx.aggregates.HierarchyBuilderRedactionBased;
import org.deidentifier.arx.aggregates.HierarchyBuilderRedactionBased.Order;
import org.deidentifier.arx.criteria.DPresence;
import org.deidentifier.arx.criteria.HierarchicalDistanceTCloseness;
import org.deidentifier.arx.criteria.KAnonymity;
import org.deidentifier.arx.criteria.RecursiveCLDiversity;
import static org.deidentifier.arx.examples.Example.printResult;
import static org.deidentifier.arx.examples.Generalisation.EuropeCountries;
import org.deidentifier.arx.metric.Metric;

/**
 * This class implements an simple example for using multiple sensitive attributes and
 * different privacy models.
 *
 * @author Fabian Prasser
 * @author Florian Kohlmayer
 */
public class AnotherTestForAnonymization extends Example {

    //countries sorted by continent
    static String[] EuropeCountries = {"Italy", "France", "Germany", "Portugal", "Greece",
                                        "Hungary", "Ireland", "England",
                                        "Holand-Nether", "Yugoslavia", "Scotland", "Poland"
                                    };
    static String[] AsiaCountries = {"Vietnam", "Thailand", "Philippines", "India", "Japan",
                                        "China", "Iran", "Cambodia","Laos", "Hong", "South-Taiwan"
                                    };
    static String[] NorthAmericaCountries = {"Dominican-Rep", "El-Salvador", "Guatemala", "Honduras","Haiti",
                                            "Jamaica","Nicaragua", "Outlying-US", "Puerto-Rico","United-States",
                                            "Trinadad&Toba",  "Columbia",  "Canada",  "Cuba","Mexico"
                                          };
    static String[] SouthAmericaCountries = {"Peru", "Ecuador"};
    
// posible marital status 
    static String[] mariedStatus = {"Married-AF-spouse","Married-civ-spouse","Married-spouse-absent","Widowed"};
    static String[] notMarriedStatus = {"Divorced" , "Never-married", "Separated"};
    
    //Education possibilities
    static String[] lessThenBachelor= {"10th","11th","12th","1st-4th","5th-6th","7th-8th","9th","Assoc-acdm","Assoc-voc"};
    static String[] bachelor = {"Bachelors"};
    static String[] moreThenBachelor = {"Doctorate","HS-grad","Masters","Preschool","Prof-school","Some-college"};
    
//Relationship
    static String[] relationshipp = {"Husband","Not-in-family","Other-relative","Own-child","Unmarried","Wife"};    
    
    private static final String FILE_NAME = "data/adult5.csv";
    private static DefaultHierarchy age = Hierarchy.create();
    private static DefaultHierarchy workclass = Hierarchy.create();
    private static DefaultHierarchy fnlwgt = Hierarchy.create();
    private static DefaultHierarchy education = Hierarchy.create();
    private static DefaultHierarchy education_num = Hierarchy.create();
    private static DefaultHierarchy marital_status = Hierarchy.create();
    private static DefaultHierarchy occupation = Hierarchy.create();
    private static DefaultHierarchy relationship = Hierarchy.create();
    private static DefaultHierarchy race = Hierarchy.create();
    private static DefaultHierarchy sex = Hierarchy.create();
    private static DefaultHierarchy capital_gain = Hierarchy.create();
    private static DefaultHierarchy capital_loss = Hierarchy.create();
    private static DefaultHierarchy hours_per_week = Hierarchy.create();
    private static DefaultHierarchy native_country = Hierarchy.create();
    private static DefaultHierarchy income = Hierarchy.create();
    private static List<Integer> sensitiveRecordList = new ArrayList<Integer>();

    private static Identification identification = new Identification();
    
    public static void main(String[] args) throws IOException {
        
        // Define data
        Data data = getData(FILE_NAME);
        //Define the Hierarchy of all attributs
        getHierarchyData(FILE_NAME);
        DataSubset subset;
        System.out.println("length data ="+data.getHandle().getNumRows());
        //subset = DataSubset.create(data, new HashSet<>(sensitiveRecordList));
        identification.generateHashs();
        identification.saveHashsInCSV();
        identification.changeIdsWithHachsInTheSCV();
        
        // Define hierarchies
        HierarchyBuilderRedactionBased<?> builderWorkclass = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        //HierarchyBuilderRedactionBased<?> builderFnlwgt = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderEducation_num = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        //HierarchyBuilderRedactionBased<?> builderMarital_status = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderOccupation = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        //HierarchyBuilderRedactionBased<?> builderRelationship = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderCapital_loss = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderCapital_gain = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        //HierarchyBuilderRedactionBased<?> builderAge = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        //HierarchyBuilderRedactionBased<?> builderEducation = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderRace = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        HierarchyBuilderRedactionBased<?> builderNativeCountry = HierarchyBuilderRedactionBased.create(Order.RIGHT_TO_LEFT,Order.RIGHT_TO_LEFT,' ','*');
        
        
        data.getDefinition().setAttributeType("id", AttributeType.INSENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("age", AttributeType.SENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("workclass", builderWorkclass);
        data.getDefinition().setAttributeType("fnlwgt", AttributeType.IDENTIFYING_ATTRIBUTE);
        data.getDefinition().setAttributeType("education",AttributeType.SENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("education-num", builderEducation_num);
        data.getDefinition().setAttributeType("marital-status", AttributeType.INSENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("occupation", builderOccupation);
        data.getDefinition().setAttributeType("relationship", AttributeType.INSENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("race", builderRace);
        data.getDefinition().setAttributeType("sex", AttributeType.INSENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("capital-gain", builderCapital_gain);
        data.getDefinition().setAttributeType("capital-loss", builderCapital_loss);
        data.getDefinition().setAttributeType("hours-per-week", AttributeType.INSENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("native-country", builderNativeCountry);
        data.getDefinition().setAttributeType("income", AttributeType.INSENSITIVE_ATTRIBUTE);
        data.getDefinition().setAttributeType("sensitive", AttributeType.INSENSITIVE_ATTRIBUTE);

        // Create an instance of the anonymizer
        ARXAnonymizer anonymizer = new ARXAnonymizer();
        ARXConfiguration config = ARXConfiguration.create();
        config.addPrivacyModel(new KAnonymity(4));
        config.addPrivacyModel(new RecursiveCLDiversity("age",2d, 2));
        config.addPrivacyModel(new HierarchicalDistanceTCloseness("education", 0.2d,education));
        //config.addPrivacyModel(new DPresence(0d, 0.06d, subset));
        config.setSuppressionLimit(0d);
        config.setQualityModel(Metric.createEntropyMetric());

        // Now anonymize
        ARXResult result = anonymizer.anonymize(data, config);

        // Print info
        printResult(result, data);

        // Process results
        if (result.getGlobalOptimum() != null) {
            System.out.println(" - Transformed data:");
            Iterator<String[]> transformed = result.getOutput(false).iterator();
            while (transformed.hasNext()) {
                System.out.print("   ");
                System.out.println(Arrays.toString(transformed.next()));
            }
        }
    }
    
    private static Data getData(String fileName) {
        DefaultData data = Data.create();
        Path pathToFile = Paths.get(fileName);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            String[] attributes = {};
            int i= 0;
            while (line != null ) {
                attributes = line.split(","); 
                //to test if we are on a sensitive record
                if(attributes[attributes.length -1].equals("1")){
                    sensitiveRecordList.add(i);
                }
                i++;
                data.add(attributes);
                line = br.readLine();
            }
            
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return data;
    }

    private static void getHierarchyData(String fileName) {
        DefaultData data = Data.create();
        Path pathToFile = Paths.get(fileName);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,StandardCharsets.US_ASCII)) {
            // read the first line from the text file
            String line = br.readLine();
            line = br.readLine();
            String[] attributes = {};
            // loop until all lines are read
            while (line != null ) {
                attributes = line.split(",");
                identification.setId(attributes[0]);
                age.add(ageHierarchy(attributes[1]));
                //workclass.add(attributes[1]);
                //fnlwgt.add(attributes[2]);
                education.add(educationHierarchy(attributes[4]));
                //education_num.add(attributes[4]);
                marital_status.add(marriedHierarchy(attributes[6]));
                //occupation.add(attributes[6]);
                //relationship.add(attributes[7]);
                race.add(raceHierarchy(attributes[9]));
                //sex.add(attributes[9]);
                //capital_gain.add(attributes[10]);
                //capital_loss.add(attributes[11]);
                //hours_per_week.add(attributes[12]);
                native_country.add(nativeCountryHierarchy(attributes[14]));
                //income.add(attributes[14]);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static String[] ageHierarchy(String age){
        int intAge = Integer.parseInt(age);
        String[] ageHirearchyAnonymisation = new String[3];
        ageHirearchyAnonymisation[0] = ""+intAge;
        ageHirearchyAnonymisation[1] = (intAge<40) ? "<=40" : ">40";
        ageHirearchyAnonymisation[2] = "*****";
        return ageHirearchyAnonymisation;
    }
    
    private static String[] marriedHierarchy(String maritalStatus){
        String[] maritalStatusHirearchyAnonymisation = new String[3];
        int i = 0;
        String res= "";
        boolean marritalStautsFound = false;
        maritalStatusHirearchyAnonymisation[0] = maritalStatus;
        while(i<mariedStatus.length && !marritalStautsFound){
            if(maritalStatus.equals(mariedStatus[i])){
                res = "Married";
                marritalStautsFound = true;                        
            }
            i++;
        }
        i= 0;
        while(i<notMarriedStatus.length && !marritalStautsFound){
            if(maritalStatus.equals(notMarriedStatus[i])){
                res = "Not married";
                marritalStautsFound = true;
            }
            i++;
        }
        maritalStatusHirearchyAnonymisation[1] = res;
        maritalStatusHirearchyAnonymisation[2] = "******";
        
        return maritalStatusHirearchyAnonymisation;
    }
    
    public static String[] raceHierarchy(String race){
        String [] raceHirearchyAnonymisation = new String[3];
        raceHirearchyAnonymisation[0] = race;
        
        if(race.equals("Black")){
                raceHirearchyAnonymisation[1] = "African";
        }else{
                if(race.equals("White")){
                    raceHirearchyAnonymisation[1] = "European";
                }else{
                    raceHirearchyAnonymisation[1] = "Other";
                }
        }
        raceHirearchyAnonymisation[2] = "*****";
        return raceHirearchyAnonymisation; 
    }
    
    public static String[] nativeCountryHierarchy(String nativeCountry){
        String [] nativeCountryHirearchyAnonymisation = new String[3];
        int i = 0;
        boolean coutryFounded = false;
        nativeCountryHirearchyAnonymisation[0] = nativeCountry;
        while(i<EuropeCountries.length && !coutryFounded){
            if(nativeCountry.equals(EuropeCountries[i])){
                nativeCountryHirearchyAnonymisation[1] = "Europe";
                coutryFounded = true;                        
            }
            i++;
        }
        i= 0;
        while(i<AsiaCountries.length && !coutryFounded){
            if(nativeCountry.equals(AsiaCountries[i])){
                nativeCountryHirearchyAnonymisation[1] = "Asia";
                coutryFounded = true;
            }
            i++;
        }
        i= 0;
        while(i<NorthAmericaCountries.length && !coutryFounded){
            if(nativeCountry.equals(NorthAmericaCountries[i])){
                nativeCountryHirearchyAnonymisation[1] = "North america";
                coutryFounded = true;
            }
            i++;
        }
        i= 0;
        while(i<SouthAmericaCountries.length && !coutryFounded){
            if(nativeCountry.equals(SouthAmericaCountries[i])){
                nativeCountryHirearchyAnonymisation[1] = "South America";
                coutryFounded = true;
            }
            i++;
        }
        nativeCountryHirearchyAnonymisation[2] = "*****";
        return nativeCountryHirearchyAnonymisation; 

    }

    public static String[] educationHierarchy(String education){
         String [] educationHirearchyAnonymisation = new String[3];
        int i = 0;
        boolean educationFounded = false;
        educationHirearchyAnonymisation[0] = education;
        while(i<lessThenBachelor.length && !educationFounded){
            if(education.equals(lessThenBachelor[i])){
                educationHirearchyAnonymisation[1] = "Less then bachelor";
                educationFounded = true;                        
            }
            i++;
        }
        i= 0;
        while(i<bachelor.length && !educationFounded){
            if(education.equals(bachelor[i])){
                educationHirearchyAnonymisation[1] = "Bachelor";
                educationFounded = true;
            }
            i++;
        }
        i= 0;
        while(i<moreThenBachelor.length && !educationFounded){
            if(education.equals(moreThenBachelor[i])){
                educationHirearchyAnonymisation[1] = "More then bachelor";
                educationFounded = true;
            }
            i++;
        }
        educationHirearchyAnonymisation[2] = "*****";
        return educationHirearchyAnonymisation;
    }
    
}
 /*
    private static int maxLengthForAllattributes(int column){
        FileWriter fileWriter = null;
        String herader ="";
        List<String> data = new ArrayList<String>();
        int max=0;
        String str="";
        Path pathToFile = Paths.get(FILE_NAME);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            line = br.readLine();
            String[] attributes = {};
            while (line != null ) {
                attributes = line.split(",");
                if(attributes[column].length()>max){
                    max = attributes[column].length();
                }
                line = br.readLine();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        try (BufferedReader br = Files.newBufferedReader(pathToFile,StandardCharsets.US_ASCII)) {
            String line = br.readLine();
            herader = line;
            line = br.readLine();
            String[] attributes = {};
            //System.out.println("max="+max);
            while (line != null ) {
                attributes = line.split(",");
                for (int i = 0; i < (max-attributes[column].length()); i++) {
                    attributes[column]+="*";
                }
                System.out.println(attributes[column]);
                int i= 1;
                for (String attribute : attributes) {
                    str+=attribute;
                    if(i!=attributes.length){
                        str+=",";
                    }
                    i++;
                }
                str+="\n";
                data.add(str);
                System.out.println("str="+str);
                str="";
                line = br.readLine();
            }
            
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        try {
            fileWriter = new FileWriter(FILE_NAME);
            fileWriter.append(herader);
            fileWriter.append("\n");
            for (String row : data) {
               fileWriter.append(row);
            }
            System.out.println("CSV file was created successfully !!!");
        } catch (IOException ex) {
            Logger.getLogger(Identification.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
        
        
        return max;
    } 
    */