package org.deidentifier.arx.examples;



import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Generalisation2 {
    
    //File name
    private static final String FILE_NAME = "data/adult4.csv";
    private static final String DESTINATION_FILE_NAME = "data/adult5.csv";
    private static final String NEW_LINE_SEPARATOR = "\n";
    
    public static void main(String... args) {
        readBooksFromCSV(FILE_NAME);
    }

    private static void readBooksFromCSV(String fileName) {
        List<String> data = new ArrayList<String>();
        List<Integer> randomIds = new ArrayList<Integer>();
        Random r = new Random();
        int low = 1;
        int high = 50000;
        Path pathToFile = Paths.get(fileName);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,
                StandardCharsets.US_ASCII)) {
            
            String line = br.readLine();
            line= "id,"+line;
            data.add(line);
            line = br.readLine();
            int res = 0;
            while (line != null ) {
                res=  r.nextInt(high-low)+low;
                while (randomIds.contains(res)) {
                    res=  r.nextInt(high-low)+low;
                }
                randomIds.add(res);  
                line=res+","+line  ;
                //System.out.println(r.nextInt(2));
                data.add(line);
                line = br.readLine();
            }
            writeCsvFile(DESTINATION_FILE_NAME, data);
            
            

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    public static void writeCsvFile(String fileName, List<String> data) {
        FileWriter fileWriter = null;
        try {
                fileWriter = new FileWriter(fileName);
                for (String attributes : data) {
                        fileWriter.append(attributes);
                fileWriter.append(NEW_LINE_SEPARATOR);
                }
                System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
                System.out.println("Error in CsvFileWriter !!!");
                e.printStackTrace();
        } finally {

                try {
                        fileWriter.flush();
                        fileWriter.close();
                } catch (IOException e) {
                        System.out.println("Error while flushing/closing fileWriter !!!");
                        e.printStackTrace();
                }

        }
    }
    }