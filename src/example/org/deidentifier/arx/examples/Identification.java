package org.deidentifier.arx.examples;


import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.deidentifier.arx.Data;
import org.deidentifier.arx.Data.DefaultData;

public class Identification {
    
    
    FileWriter fileWriter = null;
    String FILE_NAME ="data/hashsKeys.csv";
    String NEW_FILE_NAME ="data/adult6.csv";
    private  final String NEW_LINE_SEPARATOR = "\n";
    List<String> ids = new ArrayList<String>();
    HashMap<String,String> map = new HashMap<String,String>();

    
    
    
    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }
    
    public List<String> getIds() {
        return ids;
    }

    public void setId(String id) {
        this.ids.add(id);
    }
    
    public void generateHashs(){ 
        try {
            //int i =0;
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            for (String id : ids) {
                byte[] messageDigest = md.digest(id.getBytes());
                BigInteger no = new BigInteger(1, messageDigest);
                String hashtext = no.toString(16);
                while (hashtext.length() < 32) {
                    hashtext = "0" + hashtext;
                }
                //System.out.println("i"+i);
                //i++;
                map.put(id , hashtext); 
            }
        }catch (NoSuchAlgorithmException e) { 
            System.out.println("Exception thrown for incorrect algorithm: " + e); 
        } 
    }

    public void saveHashsInCSV(){
        
        try {
            fileWriter = new FileWriter(FILE_NAME);
            map.forEach((key,value)-> {
                try {
                    fileWriter.append(key+","+value);
                    fileWriter.append(NEW_LINE_SEPARATOR);
                } catch (IOException ex) {
                    Logger.getLogger(Identification.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            System.out.println("CSV file was created successfully !!!");
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
    }
    
    public void changeIdsWithHachsInTheSCV(){
       
        String herader ="";
        List<String> data = new ArrayList<String>();
        Path pathToFile = Paths.get(FILE_NAME);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,StandardCharsets.US_ASCII)) {
            herader = br.readLine();
            String line = br.readLine();
            String[] attributes = {};
            while (line != null ) {
                attributes = line.split(",");
                attributes[0] =map.get(attributes[0]);
                String str = "";
                int count =1;
                for (String attribute : attributes) {
                    str+=attribute;
                    if(attributes.length > count)
                        str+=',';
                    
                    count++;
                }
                data.add(str);
                
                line = br.readLine();
            }
            
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        try {
            fileWriter = new FileWriter(NEW_FILE_NAME);
            fileWriter.append(herader);
            fileWriter.append(NEW_LINE_SEPARATOR);
            for (String row : data) {
               fileWriter.append(row);
               fileWriter.append(NEW_LINE_SEPARATOR);
            }
            System.out.println("CSV file was created successfully !!!");
        } catch (IOException ex) {
            Logger.getLogger(Identification.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
    }
}