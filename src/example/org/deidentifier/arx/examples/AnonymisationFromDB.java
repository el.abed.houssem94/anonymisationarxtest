package org.deidentifier.arx.examples;

import java.io.IOException;
import java.sql.SQLException;
import org.deidentifier.arx.Data;
import org.deidentifier.arx.DataSource;
import org.deidentifier.arx.DataType;
import static org.deidentifier.arx.examples.Example.print;

/**
 *
 * @author houssem
 */
public class AnonymisationFromDB {
    
    
    
        private static void exampleJDBC() throws IOException,
                                      SQLException,
                                      ClassNotFoundException {
                                      
        // Load JDBC driver
        Class.forName ("com.mysql.jdbc.Driver");
        //Class.forName("org.sqlite.JDBC");
        
        // Configuration for JDBC source
        DataSource source = DataSource.createJDBCSource("jdbc:mysql://localhost:3306/AlzheimerDisease", "houssem", "00000000", "alzheimer");
        //DataSource source = DataSource.createJDBCSource("jdbc:sqlite:data/test.db", "test");
                                                        
        // Add columns
        source.addColumn(2, DataType.STRING); // zipcode (index based addressing)
        source.addColumn("gender", DataType.STRING); // gender (named addressing)
        source.addColumn("age", "renamed", DataType.INTEGER); // age (named addressing + alias name)
        
        // Create data object
        Data data = Data.create(source);
        
        // Print to console
        print(data.getHandle());
        System.out.println("\n");
    }
    
    
}
