package org.deidentifier.arx.examples;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.deidentifier.arx.AttributeType;
import org.deidentifier.arx.AttributeType.Hierarchy.DefaultHierarchy;
import org.deidentifier.arx.Data;


/**
 * Simple Java program to read CSV file in Java. In this program we will read
 * list of books stored in CSV file as comma separated values.
 * 
 * @author WINDOWS 8
 *
 */
public class Generalisation {
    
    //File name
    private static final String FILE_NAME = "data/adult3.csv";
    private static final String DESTINATION_FILE_NAME = "data/adult4.csv";

    //Delimiter used in CSV file
	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static final String FILE_HEADER = "id,age,workclass,fnlwgt,education,education-num,marital-status,"
                                                + "occupation,relationship,race,sex,capital-gain,capital-loss,"
                                                + "hours-per-week,native-country,income,sensitive";
    
    //countries sorted by continent
    static String[] EuropeCountries = {"Italy", "France", "Germany", "Portugal", "Greece", "Hungary", "Ireland", "England",
        "Holand-Nether", "Yugoslavia", "Scotland", "Poland"};
    
    static String[] AsiaCountries = {"Vietnam", "Thailand", "Philippines", "India", "Japan", "China", "Iran", "Cambodia",
        "Laos", "Hong", "South-Taiwan"};
    
    static String[] NorthAmericaCountries = {"Dominican-Rep", "El-Salvador", "Guatemala", "Honduras", "Haiti", "Jamaica",
        "Nicaragua", "Outlying-US", "Puerto-Rico", "United-States", "Trinadad&Toba",  "Columbia",  "Canada",  "Cuba",
        "Mexico"};
    
    static String[] SouthAmericaCountries = {"Peru", "Ecuador"};
    
    // posible marital status 
    static String[] mariedStatus = {"Married-AF-spouse", "Married-civ-spouse" ,"Married-spouse-absent" ,"Widowed"};
    static String[] notMarriedStatus = {"Divorced" , "Never-married", "Separated"};
    
    
    
    public static void main(String... args) {
        
        //DefaultHierarchy tt = Data.create(FILE_NAME, Charset.forName(FILE_NAME), ',',new Character("\""));
        
        
        
        readBooksFromCSV(FILE_NAME);
    }

    private static void readBooksFromCSV(String fileName) {
        //List<String[]> data = new ArrayList<String[]>();
        List<String> data = new ArrayList<String>();
        Path pathToFile = Paths.get(fileName);
        try (BufferedReader br = Files.newBufferedReader(pathToFile,
                StandardCharsets.US_ASCII)) {
            // read the first line from the text file
            //String[] attributes = {};
            String line = br.readLine();
            line+= ",sensitive";
            data.add(line);
            line = br.readLine();
            while (line != null ) {
                line+= Math.round( Math.random() )  ;
                /*attributes = line.split(",");
                int i= 1; 
                 while(i<= attributes.length-1){
                    if(attributes[i].equals("?")){
                        attributes[i] = "unknown";
                    }
                    i++;
                 }*/
                
                
                //*********************Begin age generalization**********************
                //attributes = ageGeneralisation(attributes);
                //*********************End age generalization************************
                                
                //*********************Begin Race generalization*********************
                //attributes = raceGeneralisation(attributes);
                //*********************End Race generalization***********************
                
                //*********************Begin nativeCountry generalization*************
                //attributes = nativeCountryGeneralisation(attributes);
                //*********************End nativeCountry generalization***************
                
                //*********************Begin nativeCountry generalization*************
                //attributes = maritalStatusGeneralisation(attributes);
                //*********************End nativeCountry generalization***************
                data.add(line);
                //1data.add(attributes);
                line = br.readLine();
                //System.out.print(attributes[1]+" -- "+attributes[2]+" -- "+attributes[5]+" -- "+attributes[3]);
                //for (String att : attributes) 
                    //System.out.print(att);
                //System.out.println("");
            }
            writeCsvFilee(DESTINATION_FILE_NAME, data);
            
            

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }


    public static String[] ageGeneralisation(String[] attribute){
        String [] attributes = attribute;
        if(Integer.parseInt(attributes[1])<=40){
                    attributes[1] = "age<=40";
        }else{
                    if (40<Integer.parseInt(attributes[1])&&Integer.parseInt(attributes[1])<70){
                        attributes[1] = "40<age<70";
                    }else{
                        attributes[1] = ">=70";
                    }
        }
        return attributes; 
    }
    
    public static String[] raceGeneralisation(String[] attribute){
        String [] attributes = attribute;
        if(attributes[2].equals("Black")){
                attributes[2] = "African";
        }else{
                if(attributes[2].equals("White")){
                    attributes[2] = "European";
                }else{
                    attributes[2] = "Other";
                }
        }
        return attributes; 
    }
    
    public static String[] nativeCountryGeneralisation(String[] attribute){
        String [] attributes = attribute;
        int i = 0;
                boolean coutryFounded = false;
                while(i<EuropeCountries.length && !coutryFounded){
                    if(attributes[5].equals(EuropeCountries[i])){
                        attributes[5] = "Europe";
                        coutryFounded = true;                        
                    }
                    i++;
                }
                i= 0;
                while(i<AsiaCountries.length && !coutryFounded){
                    if(attributes[5].equals(AsiaCountries[i])){
                        attributes[5] = "Asia";
                        coutryFounded = true;
                    }
                    i++;
                }
                i= 0;
                while(i<NorthAmericaCountries.length && !coutryFounded){
                    if(attributes[5].equals(NorthAmericaCountries[i])){
                        attributes[5] = "North america";
                        coutryFounded = true;
                    }
                    i++;
                }
                i= 0;
                while(i<SouthAmericaCountries.length && !coutryFounded){
                    if(attributes[5].equals(SouthAmericaCountries[i])){
                        attributes[5] = "South America";
                        coutryFounded = true;
                    }
                    i++;
                }
        return attributes; 

    }
     
    public static String[] maritalStatusGeneralisation(String[] attribute){
        String [] attributes = attribute;
        int i = 0;
        boolean maritalStatusFounded = false;
        while(i<mariedStatus.length && !maritalStatusFounded){
            if(attributes[3].equals(mariedStatus[i])){
                attributes[3] = "Married";
                maritalStatusFounded = true;
            }
            i++;
        }
        i= 0;
        while(i<notMarriedStatus.length && !maritalStatusFounded){
            if(attributes[3].equals(notMarriedStatus[i])){
                attributes[3] = "Not married";
                maritalStatusFounded = true;
            }
            i++;
        }
        
        
    return attributes; 

    } 
    
    public static void writeCsvFile(String fileName, List<String[]> data) {
        FileWriter fileWriter = null;
        try {
                fileWriter = new FileWriter(fileName);

                //Write the CSV file header
                fileWriter.append(FILE_HEADER.toString());

                //Add a new line separator after the header
                fileWriter.append(NEW_LINE_SEPARATOR);

                //Write a new student object list to the CSV file
                for (String[] attributes : data) {
                    for (int i = 0; i < attributes.length; i++) {
                        fileWriter.append(attributes[i]);
                        if(i!=(attributes.length-1)){
                            fileWriter.append(COMMA_DELIMITER);
                        }
                    }
                fileWriter.append(NEW_LINE_SEPARATOR);
                }



                System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
                System.out.println("Error in CsvFileWriter !!!");
                e.printStackTrace();
        } finally {

                try {
                        fileWriter.flush();
                        fileWriter.close();
                } catch (IOException e) {
                        System.out.println("Error while flushing/closing fileWriter !!!");
        e.printStackTrace();
                }

        }
    }
    public static void writeCsvFilee(String fileName, List<String> data) {
        FileWriter fileWriter = null;
        try {
                fileWriter = new FileWriter(fileName);

                //Write the CSV file header
                fileWriter.append(FILE_HEADER.toString());

                //Add a new line separator after the header
                fileWriter.append(NEW_LINE_SEPARATOR);

                //Write a new student object list to the CSV file
                for (String attributes : data) {
                        fileWriter.append(attributes);
                fileWriter.append(NEW_LINE_SEPARATOR);
                }



                System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
                System.out.println("Error in CsvFileWriter !!!");
                e.printStackTrace();
        } finally {

                try {
                        fileWriter.flush();
                        fileWriter.close();
                } catch (IOException e) {
                        System.out.println("Error while flushing/closing fileWriter !!!");
        e.printStackTrace();
                }

        }
    }
    
    
    
    /*
    private static Hierarchy ageHierarchy(String age){
        int intAge = Integer.parseInt(age);
        DefaultHierarchy ageH  = Hierarchy.create();
        String[] ageHirearchyAnonymisation = new String[3];
        ageHirearchyAnonymisation[0] = ""+intAge;
        ageHirearchyAnonymisation[1] = (intAge<40) ? "<=40" : ">40";
        ageHirearchyAnonymisation[2] = "*****";
        ageH.add(ageHirearchyAnonymisation);
        return ageH;
    }
    
    private static Hierarchy marriedHierarchy(String maritalStatus){
        DefaultHierarchy marriedH  = Hierarchy.create();
        String[] maritalStatusHirearchyAnonymisation = new String[3];
        int i = 0;
        String res= "";
        boolean marritalStautsFound = false;
        maritalStatusHirearchyAnonymisation[0] = ""+maritalStatus;
        while(i<mariedStatus.length && !marritalStautsFound){
            if(maritalStatus.equals(mariedStatus[i])){
                res = "Married";
                marritalStautsFound = true;                        
            }
            i++;
        }
        i= 0;
        while(i<notMarriedStatus.length && !marritalStautsFound){
            if(maritalStatus.equals(notMarriedStatus[i])){
                res = "Not married";
                marritalStautsFound = true;
            }
            i++;
        }
        maritalStatusHirearchyAnonymisation[1] = res;
        maritalStatusHirearchyAnonymisation[2] = "******";
        
        marriedH.add(maritalStatusHirearchyAnonymisation);
        return marriedH;
    }
    
    public static Hierarchy raceHierarchy(String race){
        DefaultHierarchy raceH  = Hierarchy.create();
        String [] raceHirearchyAnonymisation = new String[3];
        String res;
        raceHirearchyAnonymisation[0] = "race";
        
        if(race.equals("Black")){
                raceHirearchyAnonymisation[1] = "African";
        }else{
                if(race.equals("White")){
                    raceHirearchyAnonymisation[1] = "European";
                }else{
                    raceHirearchyAnonymisation[1] = "Other";
                }
        }
        raceHirearchyAnonymisation[2] = "*****";
        raceH.add(raceHirearchyAnonymisation);
        return raceH; 
    }
    
    public static Hierarchy nativeCountryHierarchy(String nativeCountry){
        DefaultHierarchy nativeCountryH  = Hierarchy.create();
        String [] nativeCountryHirearchyAnonymisation = new String[3];
        int i = 0;
        boolean coutryFounded = false;
        nativeCountryHirearchyAnonymisation[0] = nativeCountry;
        while(i<EuropeCountries.length && !coutryFounded){
            if(nativeCountry.equals(EuropeCountries[i])){
                nativeCountryHirearchyAnonymisation[1] = "Europe";
                coutryFounded = true;                        
            }
            i++;
        }
        i= 0;
        while(i<AsiaCountries.length && !coutryFounded){
            if(nativeCountry.equals(AsiaCountries[i])){
                nativeCountryHirearchyAnonymisation[1] = "Asia";
                coutryFounded = true;
            }
            i++;
        }
        i= 0;
        while(i<NorthAmericaCountries.length && !coutryFounded){
            if(nativeCountry.equals(NorthAmericaCountries[i])){
                nativeCountryHirearchyAnonymisation[1] = "North america";
                coutryFounded = true;
            }
            i++;
        }
        i= 0;
        while(i<SouthAmericaCountries.length && !coutryFounded){
            if(nativeCountry.equals(SouthAmericaCountries[i])){
                nativeCountryHirearchyAnonymisation[1] = "South America";
                coutryFounded = true;
            }
            i++;
        }
        nativeCountryHirearchyAnonymisation[2] = "*****";
        nativeCountryH.add(nativeCountryHirearchyAnonymisation);
        return nativeCountryH; 

    }

    public static Hierarchy eudcationHierarchy(String education){
        DefaultHierarchy educationH  = Hierarchy.create();
         String [] educationHirearchyAnonymisation = new String[3];
        int i = 0;
        boolean educationFounded = false;
        educationHirearchyAnonymisation[0] = education;
        while(i<lessThenBachelor.length && !educationFounded){
            if(education.equals(lessThenBachelor[i])){
                educationHirearchyAnonymisation[1] = "Less then bachelor";
                educationFounded = true;                        
            }
            i++;
        }
        i= 0;
        while(i<bachelor.length && !educationFounded){
            if(education.equals(bachelor[i])){
                educationHirearchyAnonymisation[1] = "Bachelor";
                educationFounded = true;
            }
            i++;
        }
        i= 0;
        while(i<moreThenBachelor.length && !educationFounded){
            if(education.equals(moreThenBachelor[i])){
                educationHirearchyAnonymisation[1] = "More then bachelor";
                educationFounded = true;
            }
            i++;
        }
        educationH.add(educationHirearchyAnonymisation);
        return educationH;
    }
    */
}




        